﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour, IUnit
{
    public int GetGoldAmount()
    {
        return GetComponent<PlayerGold>().currentGold;
    }

    public int getHealth()
    {
        return GetComponent<PlayerHealth>().currentHealth;
    }

    public int getMana()
    {
        return GetComponent<PlayerMana>().currentMana;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void SetGoldAmount(int goldAmount)
    {
        GetComponent<PlayerGold>().SaveGold(goldAmount);
    }

    public void setHealth(int healthAmount)
    {
        throw new System.NotImplementedException();
    }

    public void setMana(int manaAmount)
    {
        throw new System.NotImplementedException();
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Awake()
    {
        PlayerPrefs.DeleteAll();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F3))
        {
            Save();
        }

        //ladda
        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
    }

    private void Save()
    {
        //spara 
        Vector3 playerPosition = GetPosition();
        Debug.Log("player position " + playerPosition);
        PlayerPrefs.SetFloat("playerPositionX", playerPosition.x);
        PlayerPrefs.SetFloat("playerPositionY", playerPosition.y);

        int goldAmount = GetGoldAmount();
        PlayerPrefs.SetInt("goldAmount", goldAmount);
        Debug.Log(goldAmount + " gold");

        PlayerPrefs.Save();
        Debug.Log("Saved");
    }
    private void Load()
    {
        if (PlayerPrefs.HasKey("playerPositionX")) //om det finns data sparat
        {
            //spelarens position
            float playerPositionX = PlayerPrefs.GetFloat("playerPositionX");
            float playerPositionY = PlayerPrefs.GetFloat("playerPositionY");
            Vector3 playerPosition = new Vector3(playerPositionX, playerPositionY);
            int goldAmount = PlayerPrefs.GetInt("goldAmount", 0);

            SetPosition(playerPosition);
            Debug.Log(playerPosition);

            SetGoldAmount(goldAmount);
            Debug.Log(goldAmount + " gold");

            Debug.Log("Loaded");
        }
        else
        {
            //ingen data sparat
            Debug.Log("No save available");
        }
    }
}
