﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    public int maxHealth = 20;
    public int currentHealth;

    private SavePlayer playerSave;
    private void Start()
    {
        slider = GetComponent<Slider>();
        //detta fungerar inte av någon anledning. HP mätaren ändrar inte färg automatiskt om man inte manuellt sätter slidern som referens i inspectorn. FIXAT.
        //hade med att spelaren refererade till hp mätaren (tror jag)

        currentHealth = maxHealth;
        SetMaxHealth(maxHealth);

        playerSave = GameObject.FindObjectOfType<SavePlayer>();
        playerSave.OnLoadHealth += PlayerSave_OnLoadHealth;

        PlayerHealth playerHealth = GameObject.FindObjectOfType<PlayerHealth>();
        if (playerHealth != null)
        {
            playerHealth.OnDamage += PlayerHealth_OnDamage;
            playerHealth.OnHeal += PlayerHealth_OnHeal;
            maxHealth = playerHealth.maxHealth;
            currentHealth = playerHealth.currentHealth; //mätarens hälsa hänvisar till spelarens hälsa
        }
        else
        {
            Debug.LogError("Error with OnDamage event");
        }
    }

    private void PlayerSave_OnLoadHealth(int loadedHealth)
    {
        currentHealth = loadedHealth;
        setHealth(currentHealth);
    }

    private void PlayerHealth_OnHeal(int healAmount)
    {
        setHealth(currentHealth += healAmount);//tar nuvarande hälsan och lägger till hur mycket spelaren helats för
    }

    private void PlayerHealth_OnDamage(int damageAmount)
    {
        setHealth(currentHealth -= damageAmount); //tar nuvarande hälsan och drar av hur mycket spelaren skadats för
    }

    public void SetMaxHealth(int health)
    {
        slider.maxValue = health; //
        slider.value = health; //hp mätarens maxvärde = spelarens hälsa

        fill.color = gradient.Evaluate(1f); //ger gradient ett nummer (0 = längst till vänster, 1 = längst till höger). Gör hp mätaren grön vid max hälsa
    }

    public void setHealth(int health)
    {
        slider.value = health; //sätter mätarens värde till spelarens nuvarande hälsa

        fill.color = gradient.Evaluate(slider.normalizedValue); 
        //normalized value gör sliderna value att gå mellan 0-1. Ändrar hp mätarens färg beroende på hur mycket hp man har
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }
    }
}
