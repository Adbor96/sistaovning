﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 20;
    public int currentHealth;

    bool canHeal = true;
    public float timer = 2;

    //public HPBar healthbar;

    public event TestDamageDelegate OnDamage;
    public delegate void TestDamageDelegate(int damageAmount); //delegate som sätter hur mycket man skadas för

    public event TestHealDelegate OnHeal;
    public delegate void TestHealDelegate(int healAmount); //delegate som sätter hur mycket man helas för
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        //healthbar.SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(5);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            Heal(1);
        }

        //heal timer
        if (!canHeal)
        {
            timer -= 1 * Time.deltaTime;
            if (timer <= 0)
            {
                canHeal = true;
                timer = 2;
            }
        }
    }
    public void TakeDamage(int damageAmount) //spelaren tar skada
    {
        currentHealth -= damageAmount;
        OnDamage?.Invoke(damageAmount); //skickar ut eventet om att spelaren tagit skada

        if(currentHealth <= 0)
        {
            currentHealth = 0;
            Debug.Log("DEAD");
        }
        //healthbar.setHealth(currentHealth);
    }
    public void Heal(int healAmount)
    {
        if (canHeal)
        {
            currentHealth += healAmount;
            OnHeal?.Invoke(healAmount);
            if (currentHealth >= maxHealth)
            {
                currentHealth = maxHealth;
            }
            canHeal = false;
        }
    }
    /*public void SavePlayerHealth()
    {
        SaveSystem.SavePlayerHealth(this);
    }
    public void LoadHealth()
    {
        PlayerData data = SaveSystem.LoadPlayerHealth();
        currentHealth = data.health;
    }*/
}
