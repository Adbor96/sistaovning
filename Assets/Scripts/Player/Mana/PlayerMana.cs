﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerMana : MonoBehaviour
{
    public int maxMana = 100;
    public int currentMana;

    public float manaGainTimer = 2;
    private float _manaGainTimer;

    public event ChangeManaEvent OnChangeMana;
    public delegate void ChangeManaEvent(int manaChangeAmount); //delegate som sätter hur mycket mana man förlorar

    public delegate void LoadManaDelegate(int newManaAmount);
    public event LoadManaDelegate OnLoadMana;


    public bool manaIsFull;

    public PlayerMovement player;
    // Start is called before the first frame update
    void Start()
    {
        currentMana = maxMana;
        PlayerMana playerMana = GameObject.FindObjectOfType<PlayerMana>();
        manaIsFull = true;
        _manaGainTimer = manaGainTimer;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentMana < maxMana)
        {
            _manaGainTimer -= 1 * Time.deltaTime;
            if (_manaGainTimer <= 0)
            {
                GainMana(5);
                _manaGainTimer = manaGainTimer;
            }
        }
    }
    public void LoseMana(int manaLostAmount)
    {
        Debug.Log("Lose mana");
        currentMana -= manaLostAmount;
        OnChangeMana?.Invoke(-manaLostAmount); //skickar ut event om att mana dras av
        if (currentMana <= 0)
        {
            currentMana = 0;
            Debug.Log("Out of mana");
        }
    }
    public void GainMana(int manaGainedAmount)
    {
        Debug.Log("Gain mana");
        currentMana += manaGainedAmount;
        OnChangeMana?.Invoke(manaGainedAmount); //skickar ut event om att mana läggs till
        if (currentMana > maxMana)
        {
            currentMana = maxMana;
        }
    }
    public void LoadMana(int newManaAmount)
    {
        Debug.Log("Load Mana");
        OnLoadMana?.Invoke(newManaAmount);
    }
    /*public void SavePlayerMana()
    {
        SaveSystem.SavePlayerMana(this);
    }
    public void LoadMana()
    {
        PlayerData data = SaveSystem.LoadPlayerMana();
        currentMana = data.mana;
    }*/
}
