﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public Slider slider;
    public int maxMana = 100;
    public int currentMana;

    private PlayerMovement player;
    private SavePlayer playerSave;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        currentMana = maxMana;
        SetmaxMana(maxMana);

        PlayerMana playerMana = GameObject.FindObjectOfType<PlayerMana>();

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        player.onManaLoad += Player_onManaLoad;

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();

        playerSave = GameObject.FindObjectOfType<SavePlayer>();
        playerSave.OnLoadMana += PlayerSave_OnLoadMana;

        if (playerMana != null)
        {
            playerMana.OnChangeMana += PlayerMana_OnChangeMana;
            playerMana.OnLoadMana += PlayerMana_OnLoadMana;
            currentMana = maxMana;
            maxMana = playerMana.maxMana;
            currentMana = playerMana.currentMana;
        }
        else
        {
            Debug.LogError("Error with mana events, playerMana = null");
        }
    }

    private void PlayerSave_OnLoadMana(int loadedMana)
    {
        currentMana = loadedMana;
        SetMana(loadedMana);
    }

    private void Player_onManaLoad(int _currentMana)
    {
        Debug.Log("Load mana");
        currentMana = _currentMana;
        SetMana(currentMana);
    }

    private void PlayerMana_OnLoadMana(int newManaAmount)
    {
        Debug.Log("Load mana");
        SetMana(newManaAmount);
    }

    private void PlayerMana_OnChangeMana(int manaChangeAmount)
    {
        SetMana(currentMana += manaChangeAmount);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetmaxMana(int mana)
    {
        slider.maxValue = mana; //
        slider.value = mana; //mana mätarens maxvärde = spelarens mana
    }
    public void SetMana(int mana)
    {
        slider.value = mana;

        if (currentMana <= 0)
        {
            currentMana = 0;
        }
    }
}
