﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSaves : MonoBehaviour
{
    [SerializeField] private GameObject unitGameObject;
    private IUnit unit;

    // Start is called before the first frame update
    void Start()
    {
        unit = unitGameObject.GetComponent<IUnit>();
    }
    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Save
        if (Input.GetKeyDown(KeyCode.V))
        {
            Vector3 playerPosition = unit.GetPosition();
            Debug.Log("position " + playerPosition);
        }
    }
}
