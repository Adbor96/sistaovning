﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerMovement : MonoBehaviour 
{

    public CharacterController controller;

    public float speed = 12;

    public Ray playerRay;
    public RaycastHit playerRayHit = new RaycastHit();

    public InteracteableClass focus;

    public int mana;

    public delegate void manaLoadDelegate(int currentMana);
    public event manaLoadDelegate onManaLoad;
    //[SerializeField] private GameObject unitGameObject;
    //private IUnit unit;
    // Start is called before the first frame update
    void Start()
    {
        mana = GetComponent<PlayerMana>().currentMana;
    }
    private void Awake()
    {
        //unit = unitGameObject.GetComponent<IUnit>(); 
        mana = GetComponent<PlayerMana>().currentMana;
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        mana = GetComponent<PlayerMana>().currentMana;

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 5, Color.green);

        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("Current position: " + transform.position);
        }

        //ray
        /*if (Input.GetKeyDown(KeyCode.E))
        {
            playerRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(playerRay, out playerRayHit, 5f)) //om spelaren träffar något med sin interact-ray
            {
                Debug.Log("Interact");
                IInteracteable hitObject = playerRayHit.collider.GetComponent<IInteracteable>(); //spelarens ray hittar objekt som den kan interagera med
                if (hitObject != null)
                {
                    hitObject.Interact(); //spelaren interagerar med objektet den klickade på
                }
                return;
            }
        }*/
        //ray
        if (Input.GetKeyDown(KeyCode.E))
        {
            playerRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(playerRay, out playerRayHit, 3f)) //om spelaren träffar något med sin interact-ray
            {
                InteracteableClass interacteable = playerRayHit.collider.GetComponent<InteracteableClass>(); //spelarens ray hittar objekt som den kan interagera med
                if (interacteable != null)
                {
                    SetFocus(interacteable);
                }
                //return;
            }
            else
            {
                RemoveFocus();
            }
        }
    }
    void SetFocus(InteracteableClass newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
                focus.OnDefocus();

            focus = newFocus;
        }
        newFocus.OnFocused(transform);
    }
    void RemoveFocus()
    {
        if (focus != null)
            focus.OnDefocus();

        focus = null;
    }
    public void SavePlayer()
    {
        PlayerData saveData = new PlayerData(gameObject);
        SaveSystem.SavePlayer(saveData);
        //Debug.Log("Saving");
    }
    /*public void SavePlayerRotation()
    {
        SaveSystem.SavePlayerRotation(this);
    }*/
    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        //Debug.Log("Saved position: " + position);
        GetComponent<CharacterController>().enabled = false;
        transform.position = position;
        GetComponent<CharacterController>().enabled = true;
        Debug.Log("Current position: " + transform.position);
        Debug.Log("Load mana");
        onManaLoad?.Invoke(mana);
        gameObject.GetComponent<PlayerMana>().LoadMana(mana);

    }
    /*public void LoadPlayerRotation()
    {
        PlayerData data = SaveSystem.LoadPlayerRotation();
        Vector3 rotation;
        rotation.x = data.rotation[0];
        rotation.y = data.rotation[1];
        rotation.z = data.rotation[2];
        transform.rotation = Quaternion.Euler(rotation);
        Debug.Log("Saved rotation: " + rotation);
        Debug.Log("Current roation: " + rotation);
    } */
}
