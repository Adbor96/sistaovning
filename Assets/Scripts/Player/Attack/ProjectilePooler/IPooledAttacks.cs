﻿using UnityEngine;

public interface IPooledAttacks 
{
    void OnAttackSpawn();
}
