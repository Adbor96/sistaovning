﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FireBallAttack : MonoBehaviour
{
    AttackPooler attackPooler;
    PlayerMana mana;
    public Transform attackPos;
    public event EventHandler onPlayerFireAttack;

    /*public Rigidbody fireballPrefab;
    public Transform spawnPoint;
    public float force;*/
    // Start is called before the first frame update
    void Start()
    {
        attackPooler = AttackPooler.Instance;
        mana = GetComponent<PlayerMana>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && mana.currentMana >= 20 && Time.timeScale > 0)
        {
            /*Rigidbody FireballProjectile;
            FireballProjectile = Instantiate(fireballPrefab, spawnPoint.position, spawnPoint.rotation) as Rigidbody;
            FireballProjectile.AddForce(spawnPoint.forward * force);*/
            attackPooler.SpawnfromPool("Fireball", attackPos.position, attackPos.rotation);
            onPlayerFireAttack?.Invoke(this, EventArgs.Empty);
            mana.LoseMana(20);
        }
    }
}
