﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnit 
{
    Vector3 GetPosition(); //spelarens position
    void SetPosition(Vector3 position);

    int GetGoldAmount();

    void SetGoldAmount(int goldAmount);

    int getHealth();
    void setHealth(int healthAmount);

    int getMana();
    void setMana(int manaAmount);
}
