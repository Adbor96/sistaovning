﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCube : MonoBehaviour
{
    private HealthSystem healthSystem;
    // Start is called before the first frame update

    void Start()
    {
        //healthSystem = new HealthSystem(100);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerHealth player = other.GetComponent<PlayerHealth>();
            player.TakeDamage(10);
            //healthSystem.Damage(10);
        }
    }
}
