﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Key : MonoBehaviour, IInteracteable, ICanPickUp
{
    public event EventHandler PickupEvent;
    public UnityEvent PickUpTextNotification; //event som får texten att säga att du har plockat upp nyckeln
    public event EventHandler<OnKeyPickupEvent> OnKeyPickup;

    public class OnKeyPickupEvent : EventArgs
    {
        public GameObject KeyGameobject;
    }

    public void Interact()
    {
        Debug.Log("Interact.");
        Pickup();
    }

    public void Pickup()
    {
        Debug.Log("Got key");
        PickupEvent?.Invoke(this, EventArgs.Empty);
        PickUpTextNotification?.Invoke();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
