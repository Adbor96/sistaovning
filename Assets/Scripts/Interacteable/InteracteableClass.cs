﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteracteableClass : MonoBehaviour
{
    public float radius = 3;
    public Transform interactionTransform;

    public bool isFocused = false; //om spelar interagerar med objektet
    Transform player;

    bool hasInteracted = false;

    public virtual void Interact()
    {
        //Den här metoden ska bli överskriven
        Debug.Log("Interacting with " + transform.name);
    }

    private void Update()
    {
        if (isFocused && !hasInteracted)
        {

                Interact();
            hasInteracted = true;

        }
    }
    public void OnFocused(Transform playerTransform)
    {
        isFocused = true;
        player = playerTransform;
        hasInteracted = false;
    }

    public void OnDefocus()
    {
        isFocused = false;
        player = null;
        hasInteracted = false;
    }

    private void OnDrawGizmos()
    {
        if (interactionTransform == null)
            interactionTransform = transform;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
