﻿using UnityEngine;
using System;

public class Gold : InteracteableClass
{

    public int goldAmount; //hur mycket guld man plockar up

    public event ChangeGoldDelegate OnChangeGold;
    public delegate void ChangeGoldDelegate(int goldChangeAmount); //hur mycket guld som spelaren får eller blir av med
    public override void Interact()
    {
        base.Interact();

        PickupGold();
    }
    private void Start()
    {

    }

    // Update is called once per frame
    /*void Update()
    {
        //Update gör så att koden inte funkar av någon anledning
    }*/
    void PickupGold()
    {
        Debug.Log("Picked up " + goldAmount + " gold.");
        //lägg till i inventory
        PlayerGold player = GameObject.FindObjectOfType<PlayerGold>();
        player.GoldChanged(goldAmount);
        Destroy(gameObject);
    }
}
