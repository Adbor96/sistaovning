﻿using UnityEngine;

[CreateAssetMenu(fileName = "New item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;

    public int attack;
    public int armor;
    public int manaRegen;
    public int manaIncrease;
    public int healthRegen;
    public int healthIncrease;

    public virtual void Use()
    {
        Debug.Log("Using " + name);
    }
}
