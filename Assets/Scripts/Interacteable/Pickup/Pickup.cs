﻿using UnityEngine;

public class Pickup : InteracteableClass
{

    public Item item;
    public override void Interact()
    {
        base.Interact();

        PickupItem();
    }
    void PickupItem()
    {
        Debug.Log("Picked up " + item.name);
        bool wasPickedUp = PlayerInventory.instance.addItem(item); //bool säger om du har lagt till item i spelarens inventory
        if (wasPickedUp)
        {
            Destroy(gameObject); //"ta bort" objektet om det plockades upp
        }
    }
}
