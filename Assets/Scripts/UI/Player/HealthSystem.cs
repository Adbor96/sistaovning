﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealthSystem: MonoBehaviour
{
    private int health;
    private int healthMax;

    public event EventHandler OnHealthChanged;

    public HealthSystem(int healthMax)
    {
        this.healthMax = healthMax;
        health = healthMax;
    }
    public int GetHealth()
    {
        return health;
    }
    public float getHealthPercent()
    {
        return (float)health / healthMax;
    }
    public void Damage(int damageAmount)
    {
        health -= damageAmount;
        if (health < 0)
        {
            health = 0;
        }
            if (health > healthMax)
            {
                health = healthMax;
            }
        OnHealthChanged?.Invoke(this, EventArgs.Empty);
            if (OnHealthChanged == null)
        {
            Debug.LogError("OnHealthChanged not called");
        }

    }
    public void Heal(int healAmount)
    {
        health += healAmount;
        OnHealthChanged?.Invoke(this, EventArgs.Empty);
        if (OnHealthChanged == null)
        {
            Debug.LogError("OnHealthChanged not called");
        }
    }
}

