﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private HealthSystem healthSystem;
    public void Setup(HealthSystem healthSystem)
    {
        this.healthSystem = healthSystem;

        healthSystem.OnHealthChanged += HealthSystem_OnHealthChanged;
    }

    private void HealthSystem_OnHealthChanged(object sender, System.EventArgs e) //when the event says the health changed
    {
        transform.Find("HPBar").localScale = new Vector3(healthSystem.getHealthPercent(), 1); //change the health bar 
    }

    private void Update()
    {
        //transform.Find("HPBar").localScale = new Vector3(healthSystem.getHealthPercent(), 1); //this works, but the event doesn't. Odd. Something wrong with event calling?
    }
}
