﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public HealthBar healthBar;
    float timer = 2;
    bool isPaused = false;
    public GameObject pausePanel;
    // Start is called before the first frame update
    void Start()
    {
        //HealthSystem healthSystem = new HealthSystem(100);

        //healthBar.Setup(healthSystem);
        //Debug.Log("Health " + healthSystem.getHealthPercent());
        //healthSystem.Damage(10);
        //Debug.Log("Health " + healthSystem.getHealthPercent());
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Pause();
            }
            else
            {
                UnPause();
            }
        }
    }
    void Pause()
    {
        pausePanel.SetActive(true);
        isPaused = true;
        Time.timeScale = 0;
    }
    public void UnPause()
    {
        pausePanel.SetActive(false);
        isPaused = false;
        Time.timeScale = 1;
    }
}
