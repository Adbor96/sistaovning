﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ResolutionManager : MonoBehaviour
{
    public int width;
    public int height;
    public Text resolutionText;

    // Start is called before the first frame update
    void Start()
    {
        bool isFullScreen = true;
        if(PlayerPrefs.HasKey("resolutionHeight") && PlayerPrefs.HasKey("resolutionWidth"))
        {
            width = PlayerPrefs.GetInt("resolutionWidth");
            height = PlayerPrefs.GetInt("resolutionHeight");

        }
        if (PlayerPrefs.HasKey("fullScreenCheck"))
        {
            if(PlayerPrefs.GetInt("fullScreenCheck") == 1)
            {
                isFullScreen = true;
                resolutionText.text = isFullScreen.ToString();
            }
            else
            {
                isFullScreen = false;
                resolutionText.text = isFullScreen.ToString();
            }
        }

        Screen.SetResolution(width, height, isFullScreen);
        Debug.Log("Current height is " + PlayerPrefs.GetInt("resolutionHeight"));
        Debug.Log("Current width is " + PlayerPrefs.GetInt("resolutionWidth"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetWidth(int newWidth)
    {
        width = newWidth;
        PlayerPrefs.SetInt("resolutionWidth", width);
    }
    public void setHeight(int newHeight)
    {
        height = newHeight;
        PlayerPrefs.SetInt("resolutionHeight", height);
    }
    public void SetRes()
    {
        Screen.SetResolution(width, height, Screen.fullScreen);
        //spara upplösningen
        Debug.Log("New height is " + PlayerPrefs.GetInt("resolutionHeight"));
        Debug.Log("New width is " + PlayerPrefs.GetInt("resolutionWidth"));
    }

    public void SetFullScreen()
    {
        //toggle fullscreen
        Screen.fullScreen = !Screen.fullScreen;
        if (Screen.fullScreen == false) //den verkar inte ändra sig under samma frame
        {
            PlayerPrefs.SetInt("fullScreenCheck", 1);
            resolutionText.text = "Saved fullscreen true";
        }
        else
        {
            PlayerPrefs.SetInt("fullScreenCheck", 0);
            resolutionText.text = "Saved fullscreen false";
        }
        
        Debug.Log(Screen.fullScreen);
    }
}
