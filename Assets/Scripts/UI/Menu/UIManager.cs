﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Animator startButton;
    public Animator settingButton;
    public Animator dialog; //settings panel
    public Animator contentPanel; //about, achievments, leaderboards
    public Animator gearImage; //gear image

    public GameObject loadingScreen;
    public Slider loadSlider;
    public Text loadingProgressText;


    public Slider volumeSlider;
    public AudioSource source;

    private void Start()
    {
        if (PlayerPrefs.HasKey("volume"))
        {
            source.volume = PlayerPrefs.GetFloat("volume");
            volumeSlider.value = source.volume;
        }
        else
        {
            source.volume = volumeSlider.maxValue;
            volumeSlider.value = volumeSlider.maxValue;
        }
    }
    public void StartGame(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        loadingScreen.SetActive(true);

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex); //async håller nuvarande scenen kvar medans nästa laddas
        while (!operation.isDone) //medans nästa scen inte har laddat färdigt
        {
            float progress = Mathf.Clamp01(operation.progress / .9f); //progress är hur långt nästa scen har laddats
            loadSlider.value = progress;
            loadingProgressText.text = progress * 100 + "%";

            yield return null; //väntar tills nästa frame med att fortsätta med koden
        }
    }
    public void OpenSettings()
    {
        //hide start button
        startButton.SetBool("isHidden", true);
        //hide settings button
        settingButton.SetBool("isHidden", true);
        //make settings panel appear
        dialog.SetBool("isHidden", false);
    }
    public void CloseSettings()
    {
        //show start button
        startButton.SetBool("isHidden", false);
        //show settings button
        settingButton.SetBool("isHidden", false);
        //make settings panel disappear
        dialog.SetBool("isHidden", true);
    }
    public void ToggleMenu()
    {
        bool isHidden = contentPanel.GetBool("isHidden"); //makes the boolean "isHidden" toggleable
        contentPanel.SetBool("isHidden", !isHidden); //can toggle the "isHidden" bool on and off

        gearImage.SetBool("isHidden", !isHidden);
    }

    public void VolumeChange()
    {
        source.volume = volumeSlider.value;
        PlayerPrefs.SetFloat("volume", volumeSlider.value);
    }
}
