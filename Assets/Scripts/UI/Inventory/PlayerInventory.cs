﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    #region Singleton
    public static PlayerInventory instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of inventory found!");
        }
        instance = this;
    }

    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int maxSpace = 20;

    public List<Item> itemList = new List<Item>();

    public bool addItem(Item item) //bool om man kan lägga till ett item i inventory
    {
        if (!item.isDefaultItem)
        {
            if(itemList.Count >= maxSpace)
            {
                Debug.Log("Not enough room.");
                return false; //om inventory är fullt kan du inte plocka upp
            }
            itemList.Add(item);

            if (onItemChangedCallback != null)
            {
                onItemChangedCallback.Invoke();
            }
        }
        return true;
    }
    public void removeItem(Item item)
    {
        itemList.Remove(item);

        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }
}
