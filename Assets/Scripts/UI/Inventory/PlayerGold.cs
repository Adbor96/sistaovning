﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGold : MonoBehaviour
{

    public int currentGold;
    public event ChangeGoldDelegate OnGoldChange;
    public delegate void ChangeGoldDelegate(int goldChangeAmount);

    public delegate void SavedGoldDelegate(int savedGoldAmount);
    public event SavedGoldDelegate OnSavedGold;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GoldChanged(int goldAmountChanged) //hur mycket guld spelaren har fått eller förlorat
    {
        currentGold += goldAmountChanged;
        OnGoldChange?.Invoke(goldAmountChanged);
    }
    
    public void SaveGold(int savedGold) //hur mycket guld spelaren har när den sparar
    {
        currentGold = savedGold;
        OnSavedGold?.Invoke(savedGold);
    }
   /* public void SavePlayerGold()
    {
        SaveSystem.SavePlayerGold(this);
    }
    public void LoadGold()
    {
        PlayerData data = SaveSystem.LoadPlayerGold();
        currentGold = data.gold;
    }*/
}
