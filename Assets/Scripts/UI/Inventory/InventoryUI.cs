﻿using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    PlayerInventory inventory;
    InventorySlot[] slots;
    public GameObject inventoryUI;
    public Transform itemsParent;
    bool isActive = false;
    // Start is called before the first frame update
    void Start()
    {
        inventory = PlayerInventory.instance;
        inventory.onItemChangedCallback += UpdateUI; //update UI metod ska köras när inventory skickar eventet att inventory har ändrats   

        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (!isActive)
            {
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                isActive = true;
                Time.timeScale = 0;
                Cursor.visible = true;
            }
            else
            {
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                isActive = false;
                Time.timeScale = 1;
                Cursor.visible = false;
            }
        }
    }
    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.itemList.Count)
            {
                slots[i].AddItem(inventory.itemList[i]); //lägg till item i den itemslot som är ledig
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
