﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldCounter : MonoBehaviour
{
    public Text goldAmountText; //texten som visar hur mycket guld spelaren har
    public int currentGold; //hur mycket guld spelaren har

    private SavePlayer playerSave;
    // Start is called before the first frame update
    void Start()
    {
        PlayerGold playerGold = GameObject.FindObjectOfType<PlayerGold>();
        goldAmountText = GetComponentInChildren<Text>();

        playerSave = GameObject.FindObjectOfType<SavePlayer>();
        playerSave.OnLoadGold += PlayerSave_OnLoadGold;

        if (playerGold != null)
        {
            playerGold.OnGoldChange += PlayerGold_OnGoldChange;
            playerGold.OnSavedGold += PlayerGold_OnSavedGold;
            currentGold = playerGold.currentGold; //sätter mängden guld till lika mycket som spelaren har
            goldAmountText.text = currentGold.ToString();
        }
    }

    private void PlayerSave_OnLoadGold(int loadedGold)
    {
        currentGold = loadedGold;
        goldAmountText.text = currentGold.ToString();
    }

    private void PlayerGold_OnSavedGold(int savedGoldAmount)
    {
        currentGold = savedGoldAmount;
        goldAmountText.text = currentGold.ToString();
    }

    private void PlayerGold_OnGoldChange(int goldChangeAmount)
    {
        currentGold += goldChangeAmount;
        goldAmountText.text = currentGold.ToString();
    }

    private void LoadGold(int loadedGold)
    {
        currentGold = loadedGold;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
