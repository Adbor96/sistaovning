﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[System.Serializable]
public class SaveFile
{
    //Skapa ett objekt från filen och ställ in alla variabler när du ska spara.
    SaveFile mySave = new SaveFile();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Save()
    {
        //Därefter skriver du hela sparobjektet till en fil med en Binary formatter på detta vis:
        BinaryFormatter myBinaryFormatter = new BinaryFormatter();

        //Du behöver ha din filestream redo för att skriva till filen, t.ex. genom:
        FileStream myFileStream = File.Create(Application.persistentDataPath + "/save01.savegame");

        //Låt sedan din Binary formatter jobba genom 
        myBinaryFormatter.Serialize(myFileStream, mySave);

        //Glöm inte att stänga filestreamen efter dig med 
        myFileStream.Close();
    }
    void Load()
    {
        //Skapa en ny BinaryFormatter:
        BinaryFormatter myBinaryFormatter = new BinaryFormatter();

        //Öppna filen med en ny FileStream:
        FileStream myFileStream = File.OpenRead(Application.persistentDataPath + "/save01.savegame");

        //Använd BinaryFormattern för att göra om filen till ett Save-objekt igen och spara ner i en variabel. Tänk på att du måste typkonvertera till ditt sparobjekts typ:
        SaveFile mysave = (SaveFile)myBinaryFormatter.Deserialize(myFileStream);
    }
}
