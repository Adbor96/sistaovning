﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem 
{
    public static void SavePlayer(PlayerData data)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerSave.txt";

        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);

        stream.Close();
    }
   /*public static void SavePlayerPosition(PlayerMovement playerPos)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerSave.txt";

        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(playerPos.gameObject);
        Debug.Log("Saved position: " + data.position[0] + "," + data.position[1] + "," + data.position[2]);
        formatter.Serialize(stream, data);

        stream.Close();
    }
    public static void SavePlayerRotation(PlayerMovement playerRot)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerSave.txt";

        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(playerRot.gameObject);

        formatter.Serialize(stream, data);

        stream.Close();
    }
    public static void SavePlayerHealth(PlayerHealth playerHealth)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerSave.txt";

        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(playerHealth);

        formatter.Serialize(stream, data);

        stream.Close();
    }
    public static void SavePlayerMana(PlayerMana playerMana)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerMana.txt";

        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(playerMana);

        formatter.Serialize(stream, data);

        stream.Close();
    }
    public static void SavePlayerGold(PlayerGold playerGold)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerGold.txt";

        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(playerGold);

        formatter.Serialize(stream, data);

        stream.Close();
    }
    */
    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/playerSave.txt";
        if (File.Exists(path)) //om fil existerar i sökvägen
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
    /*public static PlayerData LoadPlayerPosition()
    {
        string path = Application.persistentDataPath + "/playerPos.txt";
        if (File.Exists(path)) //om fil existerar i sökvägen
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            Debug.Log("Loaded position: " + data.position[0] + "," + data.position[1] + "," + data.position[2]);
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
    public static PlayerData LoadPlayerRotation()
    {
        string path = Application.persistentDataPath + "/playerRot.txt";
        if (File.Exists(path)) //om fil existerar i sökvägen
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
    public static PlayerData LoadPlayerHealth()
    {
        string path = Application.persistentDataPath + "/playerHealth.txt";
        if (File.Exists(path)) //om fil existerar i sökvägen
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
    public static PlayerData LoadPlayerMana()
    {
        string path = Application.persistentDataPath + "/playerMana.txt";
        if (File.Exists(path)) //om fil existerar i sökvägen
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
    public static PlayerData LoadPlayerGold()
    {
        string path = Application.persistentDataPath + "/playerGold.txt";
        if (File.Exists(path)) //om fil existerar i sökvägen
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }*/
}
