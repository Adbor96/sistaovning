﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePlayer : MonoBehaviour
{
    // Start is called before the first frame update

    public delegate void LoadManaDelegate(int loadedMana);
    public event LoadManaDelegate OnLoadMana;

    public delegate void LoadHealthDelegate(int loadedHealth);
    public event LoadHealthDelegate OnLoadHealth;

    public delegate void LoadGoldDelegate(int loadedGold);
    public event LoadGoldDelegate OnLoadGold;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayerSaveFunction()
    {
        PlayerData saveData = new PlayerData(gameObject);
        SaveSystem.SavePlayer(saveData);
        //Debug.Log("Saving");
    }
    /*public void SavePlayerRotation()
    {
        SaveSystem.SavePlayerRotation(this);
    }*/
    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        //Debug.Log("Saved position: " + position);
        GetComponent<CharacterController>().enabled = false;
        transform.position = position;
        GetComponent<CharacterController>().enabled = true;
        Debug.Log("Current position: " + transform.position);

        GetComponent<PlayerMana>().currentMana = data.mana;
        GetComponent<PlayerHealth>().currentHealth = data.health;
        GetComponent<PlayerGold>().currentGold = data.gold;
        OnLoadMana?.Invoke(data.mana);

        OnLoadHealth?.Invoke(data.health);

        OnLoadGold?.Invoke(data.gold);
    }
}
