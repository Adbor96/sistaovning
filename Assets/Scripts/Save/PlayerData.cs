﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData 
{
    public int health;
    public int mana;
    public int gold;
    public float[] position;
    public float[] rotation;

    public PlayerData (GameObject player) //möjlig lösning: döp om funktioner till ("set " + värde man vill spara), typ SetPosition, SetGold, SetMana, SetHealth
    {
        PlayerMovement playerPosition = player.GetComponent<PlayerMovement>();
        position = new float[3];
        position[0] = playerPosition.transform.position.x;
        position[1] = playerPosition.transform.position.y;
        position[2] = playerPosition.transform.position.z;

        rotation = new float[3];
        rotation[0] = playerPosition.transform.rotation.x;
        rotation[1] = playerPosition.transform.rotation.y;
        rotation[2] = playerPosition.transform.rotation.z;

        PlayerGold playerGold = player.GetComponent<PlayerGold>();
        gold = playerGold.currentGold;

        PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();
        health = playerHealth.currentHealth;

        PlayerMana playerMana = player.GetComponent<PlayerMana>();
        mana = playerMana.currentMana;
    }
}
