﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas : MonoBehaviour
{
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        Key key = GameObject.FindObjectOfType<Key>();
        if (key != null)
        {
            key.PickupEvent += Key_PickupEvent;
        }
        else
        {
            Debug.Log("Error");
        }
    }

    private void Key_PickupEvent(object sender, EventArgs e)
    {
        StartCoroutine(DisableText());
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator DisableText()
    {
        Debug.Log("Wait to disable");
        yield return new WaitForSeconds(3);
        text.enabled = false;
    }
}
