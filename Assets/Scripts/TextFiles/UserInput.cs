﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class UserInput : MonoBehaviour
{
    public InputField userInput;
    Text previousInput;
    public InputField fileNameInput;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CreateText()
    {
        /*//var textfilen finns på datorn
        string path = Application.dataPath + "/userName.txt";
        //skapa textfil om den inte finns
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "Login log \n\n");
        }
        //filens innehåll
        string content = "Login date: " + System.DateTime.Now + "\n";
        //lägg till text i filen
        File.AppendAllText(path, content);*/
    }

    public void SaveText()
    {
        string path = Application.persistentDataPath + "/" + fileNameInput.text + ".txt"; //sökvägen filen sparas på, persistent path ger dig den hårddisk du har rättighet att spara på
        StreamWriter mySteamWriter = new StreamWriter(path); //öppnar en dataström till den sökväg man valt

        mySteamWriter.WriteLine(userInput.text);
        mySteamWriter.Close();
    }
    public void LoadText()
    {
        string path = Application.persistentDataPath + "/" + fileNameInput.text + ".txt"; //sökvägen filen sparas på, persistent path ger dig den hårddisk du har rättighet att spara på
        StreamReader myStreamReader = new StreamReader(path);

        userInput.text = myStreamReader.ReadLine(); //userinputs text blir den som står i filen vars namn du valt
        myStreamReader.Close();
    }
    public void ClearText()
    {
        userInput.text = null;
    }
}
