﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TextFile : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CreateText();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateText()
    {
        //var textfilen finns på datorn
        string path = Application.dataPath + "/log.txt";
        //skapa textfil om den inte finns
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "Login log \n\n");
        }
        //filens innehåll
        string content = "Login date: " + System.DateTime.Now + "\n";
        //lägg till text i filen
        File.AppendAllText(path, content);
    }
}
